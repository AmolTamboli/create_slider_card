//
//  ViewController.swift
//  Creating_Sliding_Card
//
//  Created by Amol Tamboli on 30/06/20.
//  Copyright © 2020 Amol_Tamboli. All rights reserved.
//

import UIKit
import CardSlider

struct Item: CardSliderItem {
    var image: UIImage
    var rating: Int?
    var title: String
    var subtitle: String?
    var description: String?
}

class ViewController: UIViewController,CardSliderDataSource {

    @IBOutlet var myButton: UIButton!
    
    var data = [Item]()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        myButton.backgroundColor = .link
        myButton.setTitleColor(.white, for: .normal)
        data.append(Item(image: UIImage(named: "img1")!, rating: 1, title: "Nature Love", subtitle: "Beautiful", description: "You Can Save Image"))
        data.append(Item(image: UIImage(named: "img2")!, rating: 1, title: "Beautiful Nature", subtitle: "Awsome", description: "You Can Save Image"))
        data.append(Item(image: UIImage(named: "img3")!, rating: 1, title: "Love", subtitle: "Nature Love", description: "You Can Save Image"))
        data.append(Item(image: UIImage(named: "img4")!, rating: 1, title: "Beautiful", subtitle: "Beautiful", description: "You Can Save Image"))
        data.append(Item(image: UIImage(named: "img5")!, rating: 1, title: "Nature Love", subtitle: "Beautiful Nature", description: "You Can Save Image"))
        data.append(Item(image: UIImage(named: "img6")!, rating: 1, title: "Nature Love", subtitle: "Awsome", description: "You Can Save Image"))
        data.append(Item(image: UIImage(named: "img7")!, rating: 1, title: "Nature Love", subtitle: "Beautiful Nature", description: "You Can Save Image"))
        data.append(Item(image: UIImage(named: "img8")!, rating: 1, title: "Nature Love", subtitle: "Awsome", description: "You Can Save Image"))
        data.append(Item(image: UIImage(named: "img9")!, rating: 1, title: "Nature Love", subtitle: "Beautiful Nature", description: "You Can Save Image"))
        data.append(Item(image: UIImage(named: "img10")!, rating: 1, title: "Love", subtitle: "Awsome", description: "You Can Save Image"))
        data.append(Item(image: UIImage(named: "img11")!, rating: 1, title: "Nature Love", subtitle: "Beautiful", description: "You Can Save Image"))
        data.append(Item(image: UIImage(named: "img12")!, rating: 1, title: "Love", subtitle: "Beautiful Nature", description: "You Can Save Image"))
        data.append(Item(image: UIImage(named: "img13")!, rating: 1, title: "Nature Love", subtitle: "Awsome", description: "You Can Save Image"))
        data.append(Item(image: UIImage(named: "img14")!, rating: 1, title: "Nature Love", subtitle: "Beautiful Nature", description: "You Can Save Image"))
        data.append(Item(image: UIImage(named: "img15")!, rating: 1, title: "Love", subtitle: "Awsome", description: "You Can Save Image"))
    }
    
    @IBAction func didTappedBtn(){
        //MARK:- Present card slider
        let vc = CardSliderViewController.with(dataSource: self)
        vc.title = "Welcome"
        vc.modalPresentationStyle = .fullScreen
        present(vc,animated: true)
    }

    func item(for index: Int) -> CardSliderItem {
        return data[index]
    }
    
    func numberOfItems() -> Int {
        return data.count
    }
}

